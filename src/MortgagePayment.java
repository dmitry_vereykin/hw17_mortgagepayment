/**
 * Created by Dmitry on 7/2/2015.
 */
import java.util.Scanner;
import java.text.DecimalFormat;

public class MortgagePayment {
    public static void main(String[] args){
        Scanner keyboard = new Scanner(System.in);

        System.out.print("What is the amount of the loan? --> ");
        int loan = keyboard.nextInt();

        System.out.print("What is the annual rate (per cent) of interest? --> ");
        double annualRate = keyboard.nextDouble();

        System.out.print("How many years is the loan for? --> ");
        int years = keyboard.nextInt();
         
        keyboard.close();
        
        double monthRate = (annualRate / 100) / 12;
        int months = years * 12;

        double base = 1 + monthRate;
        double payment = (double) loan * (monthRate / (1 - (Math.pow(base, -months))));

        DecimalFormat monthPay = new DecimalFormat("#,##0.00");
        System.out.println("------------------------------------------------------");
        System.out.println("The monthly payment will be $" + monthPay.format(payment));
    }
}
